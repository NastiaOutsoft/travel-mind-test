package com.example.nastia.travelmindtest;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView troubleLogin =(TextView)findViewById(R.id.troubleLogin);
        troubleLogin.setClickable(true);
        troubleLogin.setMovementMethod(LinkMovementMethod.getInstance());
        String text = "<a href='http://www.google.com'> Trouble in login? </a>";
        troubleLogin.setText(Html.fromHtml(text));

        TextView signUp =(TextView)findViewById(R.id.signUp);
        signUp.setClickable(true);
        signUp.setMovementMethod(LinkMovementMethod.getInstance());
        String text2 = "<a href='http://www.google.com'> Sign up now </a>";
        signUp.setText(Html.fromHtml(text2));

        ImageView facebookLogin = (ImageView) findViewById(R.id.facebook);

        facebookLogin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                Intent browserIntent = new Intent("android.intent.action.VIEW",

                        Uri.parse("http://www.google.com"));

                startActivity(browserIntent);

            }

        });
        ImageView twitterLogin = (ImageView) findViewById(R.id.twitter);

        twitterLogin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                Intent browserIntent = new Intent("android.intent.action.VIEW",

                        Uri.parse("http://www.google.com"));

                startActivity(browserIntent);

            }

        });

    }
}
